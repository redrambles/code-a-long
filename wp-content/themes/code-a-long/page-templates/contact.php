<?php
/*
Template Name: Contact Page
*/

get_header(); ?>

<?php
$errors = array();
$success = '';
$show_form = true;

if ($_POST) {
//    var_dump($_POST);

    // do some form field validation here
    // if a form field fails validation,
    // add a message to the $errors array
    if (!$_POST['user_name']) {
        $errors['user_name'] = 'Please enter your name.';
    }

    if (!$_POST['user_email']) {
        $errors['user_email'] = 'Please enter your email address.';
    }

    if (!$_POST['user_subject']) {
        $errors['user_subject'] = 'Please enter a subject line.';
    }

    if (!$_POST['user_message']) {
        $errors['user_message'] = 'Please enter a message.';
    }

    // successfully submitted form
    if (!$errors) {
        // send the email!
        //wp_mail();
        $success = 'Thanks for your message!';
        $show_form = false;
    }
} ?>

<main id="main" class="site-main" role="main">
    <?php while ( have_posts() ) : the_post(); ?>

    <div class="content-wrapper contact">
        <div class="one-third">
            <?php the_content(); ?>
        </div>

        <div class="two-third">
            <?php
            if ($errors) {
                echo '<h1>Oops!</h1>';
                foreach ($errors as $e) {
                    echo '<p>' . $e . '</p>';
                }
            } elseif ($success) {
                echo '<h1>' . $success . '</h1>';
            }
            ?>

            <?php if ($show_form) { ?>
            <form method="post" action="">
                <label for="user_name">Name</label>
                <input type="text" name="user_name" id="user_name" />

                <label for="user_email">Email</label>
                <input type="email" name="user_email" id="user_email" />

                <label for="user_subject">Subject</label>
                <input type="text" name="user_subject" id="user_subject" />

                <label for="user_message">Message</label>
                <textarea name="user_message" id="user_message"></textarea>

                <input type="submit" name="submit" value="Send Your Message" />
            </form>
            <?php } ?>
        </div>

        <?php endwhile; // end of the loop. ?>
        <div class="clearfix"></div>
</main>

<?php get_footer(); ?>
